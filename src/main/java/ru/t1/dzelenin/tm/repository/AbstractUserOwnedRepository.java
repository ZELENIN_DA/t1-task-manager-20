package ru.t1.dzelenin.tm.repository;

import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        final List<M> result = new ArrayList<>();
        for (final M m : records) {
            if (userId.equals(m.getUserId())) result.add(m);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        for (M m : records) {
            if (!id.equals(m.getId())) continue;
            if (!userId.equals(m.getUserId())) continue;
            return m;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M removeById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(final String userId, final M model) {
        return removeById(userId, model.getId());
    }

}


