package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.api.service.IProjectService;
import ru.t1.dzelenin.tm.api.service.IProjectTaskService;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.enumerated.Role;


public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

