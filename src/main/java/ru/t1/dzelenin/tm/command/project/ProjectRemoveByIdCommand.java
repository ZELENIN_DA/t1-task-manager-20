package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
